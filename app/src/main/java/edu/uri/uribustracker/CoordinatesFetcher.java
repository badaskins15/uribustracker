package edu.uri.uribustracker;
import java.util.Observable;
import java.lang.Runnable;
import java.util.ArrayList;
import java.net.*;
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;
import org.json.*;
import java.util.concurrent.*;
import android.util.Log;

/**
 * Created by ray on 4/17/16.
 *
 * The CoordinatesFetcher class creates an array list of all of the bus
 * data that we have for each bus.
 * It uses this list to determine the exact location of each bus along
 * the routes.
 * It sends requests to the server to retrieve the data for all of the buses.
 *
 * @author emilyhendricks
 */
public class CoordinatesFetcher extends Observable {
    /**
     * Instantiate an array to store all the bus data, a timer and several booleans to
     * keep track of when the application is running.
     */
    private static final String TAG = CoordinatesFetcher.class.getSimpleName();
    private ArrayList<BusData> mBusData;
    private ExecutorService mPool;
    private Timer mTimer;
    private Future mFuture;
    private boolean running;
    private boolean errorFlag = false;
    public boolean hadError() {
        return errorFlag;
    }
    private Thread.UncaughtExceptionHandler mUncaughtExceptionHandler;

    /**
     * The CoordinateFetcher function gets the coordinates of a bus
     * and changes them when needed.
     * @param interval
     */
    public CoordinatesFetcher(int interval) {
        super();
        mPool = Executors.newFixedThreadPool(2);
        final CoordinatesFetcher self = this;
        mTimer = new Timer(true);
        mFuture = fetch();
        /**
         * Gets the bus data and changes the current bus data
         * accordingly.
         */
        mTimer.scheduleAtFixedRate(new TimerTask() {
            /**
             * Changes the coordinates fetched when needed while
             * the application is running
             */
            public void run() {
                if (running) return;
                running = true;
                try {
                    mBusData = (ArrayList<BusData>) mFuture.get();
                    setChanged();
                    notifyObservers(mBusData);
                    clearChanged();
                    errorFlag = false;
                } catch (Exception e) {
                    errorFlag = true;
                }
                mFuture = fetch();
                running = false;
            }
        }, 0, interval);
    }

    /**
     * The Future fetch function gets the information of all the
     * different buses and add them to the busData.
     * @return
     */
    public Future fetch() {
        return mPool.submit(new Callable() {
            public ArrayList<BusData> call() throws Exception {
                /**
                 * get the bus information from uri
                 */
                    String tmp;
                    URL url = new URL("http://bus.apps.uri.edu/paraVehicle.php");
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    StringBuilder jsonBuilder = new StringBuilder();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    while ((tmp = reader.readLine()) != null) {
                        jsonBuilder.append(tmp);
                    }
                    JSONArray array = new JSONArray(jsonBuilder.toString());
                    mBusData = new ArrayList<BusData>();
                    BusData ref;
                    JSONObject bus;
                    for (int i = 0; i < array.length(); ++i) {
                        /**
                         * add the bus information to the bus data
                         */
                        ref = new BusData();
                        bus = array.getJSONObject(i);
                        ref.latitude = bus.getDouble("Latitude");
                        ref.longitude = bus.getDouble("Longitude");
                        ref.time = bus.getString("Time");
                        ref.route = bus.getString("Route");
                        ref.vehicle = bus.getString("Vehicle");
                        mBusData.add(ref);
                    }
                return mBusData;
            }
        });

    }
}
