package edu.uri.uribustracker;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.HashMap;
import android.graphics.Color;
import java.util.Observer;
import java.util.Observable;
import android.location.LocationManager;
import java.util.ArrayList;
import java.util.Iterator;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.Toast;
import java.util.HashSet;
import android.content.Context;

/**
 * The MapActivity class is where almost all of the communication occurs.
 * This class sets up the coordinates for the bus stops and buses.
 * It also draws the bus icon and the routes on the map.
 * From this class, we launch all of the main features which allows the
 * the application to run.
 *
 * @author emilyhendricks
 */

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, Observer {
    /**
     * A google map, timer and coordinate fetcher are all instantiated.
     */
    private GoogleMap mMap;
    public CoordinatesFetcher mCoordinatesFetcher;
    public Timer mTimer;
    public volatile boolean isDrawing;

    /**
     *Set the route and coordinates
     */
    static {
        double coords[] = {0, 0, 1, 1};
        Route r = new Route(coords, 1, 16.0f);
        LatLng l = new LatLng(2, 2);
        Route.InterpolationResult result = r.closestPoint(l);
        Util.printLatLong(result.point);
    }

    /**
     *
     * The BusStop function ensures that the bus stops are properly labeled,
     * at the correct coordinates and displayed as being part of the
     * correct route.
     */
    private static BusStop mBusStops[] = {
            new BusStop("plains rd. lot", 41.490843, -71.539002, "red"),
            new BusStop("flag road lot", 41.49148596, -71.53440177, "red"),
            new BusStop("flag road & BF road", 41.49091537, -71.53046429, "red"),
            new BusStop("flag road & CBLS", 41.49069034, -71.52816832, "red"),
            new BusStop("flag road & greenhouse rd", 41.49051354, -71.52620494, "red"),
            new BusStop("upper & east alumni rd", 41.48838381, -71.52513206, "red"),
            new BusStop("greenhouse & east alumni rd", 41.4884481, -71.52659118, "red"),
            new BusStop("chafee & east alumni", 41.48822307, -71.52811468, "red"),
            new BusStop("lib & east alumni", 41.48815877, -71.52927339, "red"),
            new BusStop("totell & west alumni", 41.48901871, -71.53664142, "red"),
            new BusStop("plains rd lot & west alumni", 41.4892116, -71.53814882, "red"),
            new BusStop("Plains rd. Lot", 41.490843, -71.539002, "blue"),
            new BusStop("flag road lot", 41.49148596, -71.53440177, "blue"),
            new BusStop("flag road & BF road", 41.49091537, -71.53046429, "blue"),
            new BusStop("lippit & chapel rd.", 41.48713004, -71.52551696, "blue"),
            new BusStop("flag road & CBLS", 41.49069034, -71.52816832, "blue"),
            new BusStop("flag road & greenhouse rd", 41.49051354, -71.52620494, "blue"),
            new BusStop("upper & east alumni rd", 41.48838381, -71.52513206, "blue"),
            new BusStop("greenhouse & east alumni rd", 41.4884481, -71.52659118, "blue"),
            new BusStop("chafee & east alumni", 41.48822307, -71.52811468, "blue"),
            new BusStop("lib & east alumni", 41.48815877, -71.52927339, "blue"),
            new BusStop("totell & west alumni", 41.48901871, -71.53664142, "blue"),
            new BusStop("plains rd lot & west alumni", 41.4892116, -71.53814882, "blue"),
            new BusStop("upper college & fortin", 41.48571953, -71.52595684, "blue"),
            new BusStop("ranger & parking lot", 41.48566126, -71.52656034, "blue"),
            new BusStop("ranger & lower college", 41.4856914, -71.52897969, "blue"),
            new BusStop("union circle", 41.48428287, -71.52926132, "blue"),
            new BusStop("campus ave & quarry rd.", 41.48395333, -71.5304254, "blue"),
            new BusStop("BF Dining hall", 41.4854322, -71.53175846, "blue"),
            new BusStop("Browning & Health Services", 41.48586621, -71.53263018, "blue"),
            new BusStop("Weldin & frat circle", 41.48351127, -71.53337851, "blue"),
            new BusStop("keaney rd & frat circle", 41.48391716, -71.53519437, "blue"),
            new BusStop("keaney rd next to gym", 41.48472693, -71.53605267, "blue"),
            new BusStop("keaney rd lot", 41.48396941, -71.53641745, "blue"),
            new BusStop("keaney rd lot near 138", 41.4827236, -71.53647378, "blue"),
            new BusStop("frat circle bottom left lot near 138", 41.48253873, -71.53431192, "blue"),
            new BusStop("frat circle bottom right lot near 138", 41.48216699, -71.53243437, "blue"),
            new BusStop("frat circle front of hillel center", 41.48288435, -71.53194621, "blue")
    };
    @Override
    /**
     *
     * onCreate defines what the map should be doing upon creation
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapActivity.context = getApplicationContext();
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    /**
     * Stores the bus data in a hash map and array
     */
    public HashMap<String, Bus> mBuses;
    public ArrayList<BusData> mBusData;

    /**
     * The drawBuses function draws the buses
     */
    public void drawBuses(int steps) {
        drawBuses(steps, 0);
    }


    public int mNextIteration;

    /**
     * drawBusesAtOnce adds all the buses being displayed on the map.
     */
    public void drawBusesAtOnce() {
        for (Iterator<String> it = mBuses.keySet().iterator(); it.hasNext(); ) {
            String key = it.next();
            final Bus ref = mBuses.get(key);
            runOnUiThread(new Runnable() {
                public void run() {
                    if (ref.marker == null) {
                        ref.marker = mMap.addMarker(new MarkerOptions().title(ref.vehicle).icon(BitmapDescriptorFactory.fromResource(R.mipmap.bus)).position(ref.interpolatedPos.result.point));
                    } else {
                        ref.marker.setPosition(ref.interpolatedPos.result.point);
                    }
                }
            });
        }

    }

    /**
     * Redraws and draws the individual buses
     * @param steps
     * @param n
     */
    public void drawBuses(final int steps, final int n) {
        if (n == steps) return;
        boolean go = false;
        synchronized(mBuses) {
            for (Iterator<String> it = mBuses.keySet().iterator(); it.hasNext(); ) {
                String key = it.next();
                if (mBuses.get(key).pendingRedraw) {
                    go = true;
                    break;
                }
            }
        }
        if (!go) return;
        synchronized(mBuses) {
            for (Iterator<String> it = mBuses.keySet().iterator(); it.hasNext(); ) {
                String key = it.next();
                final Bus ref = mBuses.get(key);
                if (n == 0 && ref.lastInterpolatedPos.route == null) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (ref.marker == null) {
                                ref.marker = mMap.addMarker(new MarkerOptions().title(ref.vehicle).icon(BitmapDescriptorFactory.fromResource(R.mipmap.bus)).position(ref.interpolatedPos.result.point));
                            } else {
                                ref.marker.setPosition(ref.interpolatedPos.result.point);
                            }
                        }
                    });
                }
                if (ref.lastInterpolatedPos.route != null && ref.pendingRedraw) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (ref.marker == null) {
                                ref.marker = mMap.addMarker(new MarkerOptions().title(ref.vehicle).icon(BitmapDescriptorFactory.fromResource(R.mipmap.bus)).position(ref.mostLikelyRoute().pointAlongPath(ref.mostLikelyRoute().closestPoint(ref.lastInterpolatedPos.result.point), ref.interpolatedPos.result, n + 1, steps)));
                            } else {
                                ref.marker.setPosition(ref.mostLikelyRoute().pointAlongPath((ref.lastInterpolatedPos = new Route.DynamicInterpolationResult(ref.mostLikelyRoute(), ref.mostLikelyRoute().closestPoint(ref.lastInterpolatedPos.result.point))).result, ref.interpolatedPos.result, n + 1, steps));
                            }
                            if (n == steps - 1) ref.pendingRedraw = false;
                        }
                    });
                }
            }
        }
        mNextIteration = n + 1;
        mTimer.schedule(new TimerTask() {
            public void run() {
                if (mNextIteration != n + 1) return;
                drawBuses(steps, n + 1);
            }
        }, 20);
    }
    public static Context context;
    @Override
    /**
     * update function synchronizes the application data
     * with the most recent data available.
     */
    public void update(Observable o, Object obj) {
        /**
         * call coordinatesFetcher to get current bus
         * information.
         */
        if (o == mCoordinatesFetcher) {
            mBusData = (ArrayList<BusData>) obj;
            /**
             * remove all the old data
             */
            if (mBuses != null) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        HashSet<String> toRemove;
                        synchronized (mBuses) {
                            toRemove = Util.setCopy(mBuses.keySet());
                        }
                        synchronized (toRemove) {
                            toRemove.removeAll(BusData.pluckRoutes(mBusData));
                        }
                        for (Iterator<String> it = toRemove.iterator(); it.hasNext(); ) {
                            String key;
                            Bus ref = mBuses.get(key = it.next());
                            if (ref.marker != null) {
                                ref.marker.remove();
                            }
                            synchronized (mBuses) {
                                mBuses.remove(key);
                            }
                        }
                    }
                });
            } else {
                mBuses = new HashMap<String, Bus>();
            }
            /**
             * Adds in the new bus information
             */
            for (Iterator<BusData> it = mBusData.iterator(); it.hasNext(); ) {
                BusData bus = it.next();
                if (mBuses.containsKey(bus.vehicle) == false) {
                    LatLng realLoc = new LatLng(bus.latitude, bus.longitude);
                    Route.DynamicInterpolationResult interpLoc = Route.closestPointAllRoutes(realLoc);
                    Bus entry = new Bus();
                    entry.realPos = realLoc;
                    entry.setInterpolatedPos(interpLoc);
                    entry.lastInterpolatedPos = new Route.DynamicInterpolationResult(null, new Route.InterpolationResult(new LatLng(0, 0), 0, 0));
                    if (entry.lastInterpolatedPos.result.point.latitude != entry.interpolatedPos.result.point.latitude || entry.lastInterpolatedPos.result.point.longitude != entry.interpolatedPos.result.point.longitude) {
                        entry.pendingRedraw = true;
                    }
                    entry.route = bus.route;
                    entry.vehicle = bus.vehicle;
                    entry.time = bus.time;
                    try {
                        entry.lastUpdate = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm").parse(bus.time);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (new java.util.Date().getTime() - entry.lastUpdate.getTime() > 100000) {
                        entry.pendingRedraw = false;
                    }
                    mBuses.put(bus.vehicle, entry);
                } else {
                    Bus entry = mBuses.get(bus.vehicle);
                    entry.realPos = new LatLng(bus.latitude, bus.longitude);
                    entry.lastInterpolatedPos = entry.interpolatedPos;
                    entry.setInterpolatedPos(Route.closestPointAllRoutes(entry.realPos));
                    if (entry.lastInterpolatedPos.result.point.latitude != entry.interpolatedPos.result.point.latitude || entry.lastInterpolatedPos.result.point.longitude != entry.lastInterpolatedPos.result.point.longitude) {
                        entry.pendingRedraw = true;
                    }
                    entry.time = bus.time;
                    try {
                        entry.lastUpdate = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm").parse(bus.time);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (new java.util.Date().getTime() - entry.lastUpdate.getTime() > 100000) {
                        entry.pendingRedraw = false;
                    }
                    entry.vehicle = bus.vehicle;
                }
            }
            drawBuses(50);
        } else if (o == mLocationFetcher) {
            Util.printLatLong((LatLng) obj);
        }
    }
    public void drawStops() {
        for (int i = 0; i < mBusStops.length; ++i) {
            if (RouteSelector.isSelected(mBusStops[i].route)) mMap.addMarker(new MarkerOptions().icon(mBusStops[i].getIcon()).position(new LatLng(mBusStops[i].latitude, mBusStops[i].longitude)).title(mBusStops[i].name));
        }
    }/**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near URI.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    public GPSLocationFetcher mLocationFetcher;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        com.google.android.gms.maps.GoogleMap
        mMap = googleMap;
        mTimer = new Timer();
        RouteSelector.setMapActivity(this);
        mCoordinatesFetcher = new CoordinatesFetcher(2);
        mCoordinatesFetcher.addObserver(this);
        mLocationFetcher = new GPSLocationFetcher((LocationManager) getSystemService(LOCATION_SERVICE));
        mLocationFetcher.addObserver(this);
        // Add a marker URI and move the camera
        LatLng uri = new LatLng(41.486473, -71.531843);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(uri, 15.0f));
        RouteSelector.select(RouteSelector.RED_ROUTE | RouteSelector.BLUE_ROUTE);
        mMap.addPolyline(Route.getHillClimberRoute().getPolylineOptions());
        mMap.addPolyline(Route.getBlueRoute().getPolylineOptions());
        drawStops();
    }


}
