package edu.uri.uribustracker;

/**
 * Created by ray on 4/17/16.
 *
 * This class allows the user to select which route they to appear on the map.
 * It gives the user the ability to toggle between making the Hill Climber
 * and the Blue Line routes, along with the shuttle busses on those routes visible.
 * This feature has not been completed.
 *
 *  @author emilyhendricks
 */
public class RouteSelector{
    /**
     * MapActivity being where most of the functionality occurs, is called upon.
     *
     */
    public static MapActivity mMapActivity;
    public static void setMapActivity(MapActivity a) {
        mMapActivity = a;
    }

    /**
     * The two different routes are created
     */
    static int selection = 0;
    static int RED_ROUTE = 0x01;
    static int BLUE_ROUTE = 0x02;

    /**
     * The following two functions allow the route to be
     * unselected.
     */
    static void unselect() {
        selection = 0;
        if (mMapActivity.mBuses != null) mMapActivity.update(mMapActivity.mCoordinatesFetcher, mMapActivity.mBuses);
    }
    static void unselect(int m) {
        selection &= ~m;
        if (mMapActivity.mBuses != null) mMapActivity.update(mMapActivity.mCoordinatesFetcher, mMapActivity.mBuses);
    }

    /**
     * The select function enables a route to be selected
     * @param m
     */
    static void select(int m) {
        selection |= m;
        if (mMapActivity.mBuses != null) mMapActivity.update(mMapActivity.mCoordinatesFetcher, mMapActivity.mBuses);
    }

    /**
     * The routeToEnum function determines if the blue route is currently selected or
     * the red route is.
     * @param r route
     * @return
     */
    private static int routeToEnum(Route r) {
        if (r == Route.getBlueRoute()) {
            return BLUE_ROUTE;
        } else if (r == Route.getHillClimberRoute()) {
            return RED_ROUTE;
        }
        return 0;
    }
    /**
     * The isSelected function checks to see if there has been any route that is selected.
     */
    static boolean isSelected(Route r) {
        if ((routeToEnum(r) & selection) != 0) {
            return true;
        }
        return false;
    }
}
